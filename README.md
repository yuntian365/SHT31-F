# 数字温湿度传感器
![输入图片说明](https://images.gitee.com/uploads/images/2020/0529/085415_ad44f999_5119709.jpeg "featured.jpg")
### 介绍
Gravity: SHT31数字温湿度传感器采用业内知名的瑞士Sensirion公司推出的新一代SHT31-F温湿度传感器芯片。得益于Sensirion的CMOSens®技术，高集成度电容式测湿元件和能隙式测温元件，SHT31-F能够提供极高的可靠性和出色的长期稳定性，具有功耗低、反应快、抗干扰能力强等优点。IIC通讯，兼容3.3V/5V，可以非常容易的集成到智能楼宇、天气站、仓库存储、养殖、孵化等应用场景中。
SHT31-F在SHT3x系列中属于标准版，相比上一代精度更高。传感器在0%RH~100%RH（25℃时）误差仅为±2%RH，传感器在0℃-90℃（典型值）误差仅为±0.2℃。
这款芯片上面还有一层IP67的PTFE膜，可防止传感器开孔接触灰尘，因此允许传感器在恶劣环境条件下使用，如密切接触灰尘可能对传感器的精准性具有影响的地方。由于最小封装和膜的高水气渗透性，相对湿度和温度信号的响应时间与没加膜的传感器所实现的相同。虽然，保护膜可完美防止灰尘的进入，但在一般情况下它不能防止挥发性化学物质的污染。



### 安装教程

Mind+ 库地址： Gitee 地址（适合国内用户）：https://gitee.com/yuntian365/SHT31-F

Mind+ 软件下载地址：http://mindplus.cc

Mind+扩展库教程：https://mindplus.dfrobot.com.cn/extensions-user

### 使用说明

#### 1.  Blocks

![输入图片说明](https://images.gitee.com/uploads/images/2020/0529/102750_31d2a749_5119709.png "blocks.png")

#### 2.  示例

![输入图片说明](https://images.gitee.com/uploads/images/2020/0529/102819_cc0a8ba1_5119709.png "example.png")

#### 3.  生成代码

![输入图片说明](https://images.gitee.com/uploads/images/2020/0529/102838_cce78d21_5119709.png "code.png")

### 硬件支持
Leonardo

Microbit（已完成测试）
