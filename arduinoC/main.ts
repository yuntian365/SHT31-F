
//% color="#AA278D" iconWidth=50 iconHeight=40
namespace DFRobot_SHT3x {
    //% block="get TemperatureC" blockType="reporter"
    export function getTemperatureC(parameter: any, block: any) {
        
        Generator.addInclude('DFRobot_SHT3x', '#include <DFRobot_SHT3x.h>');
        Generator.addObject(`sht3x`, `DFRobot_SHT3x`, `sht3x;`);
        Generator.addSetup(`sht3x.begin`, `sht3x.begin();`);
        Generator.addCode(`sht3x.getTemperatureC();`);
    }

    //% block="get HumidityRH" blockType="reporter"
    export function getHumidityRH(parameter: any, block: any) {
        
        Generator.addInclude('DFRobot_SHT3x', '#include <DFRobot_SHT3x.h>');
        Generator.addObject(`sht3x`, `DFRobot_SHT3x`, `sht3x;`);
        Generator.addSetup(`sht3x.begin`, `sht3x.begin();`);
        Generator.addCode(`sht3x.getHumidityRH();`);
    }


    


}
